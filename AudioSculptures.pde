import processing.pdf.*;
import processing.opengl.*;
import processing.dxf.*;

import ddf.minim.*;
import ddf.minim.analysis.*;

import unlekker.util.*;
import unlekker.modelbuilder.*;
import unlekker.modelbuilder.filter.*;
import unlekker.modelbuilder.UVertexList;

import controlP5.*;


import wblut.math.*;
import wblut.processing.*;
import wblut.core.*;
import wblut.hemesh.*;
import wblut.geom.*;


import java.util.Iterator;


import javax.swing.*;
import java.io.*;

float lastSecond = 0;
int totalMillis = 0;

Minim minim;
AudioPlayer song;
FFT fft;
AudioMetaData meta;

UNav3D nav;

UGeometry model;

Boolean isPaused = true;

float bufferSize;

HE_Mesh mesh,originalMesh;
WB_Render render;

boolean record,recordDXF = false;

boolean wireframe = false;

public USimpleGUI gui;

static final int CUBE = 0;
static final int CYLINDER = 1;
static final int TORUS = 2;
static final int SPHERE = 3;

void setup() {
  size(1280,720, OPENGL);

  minim = new Minim(this);

  initGUI();

  nav=new UNav3D(this);
  nav.trans.set(width/2,height/2,0);

  originalMesh = new HE_Mesh();

  addBaseMesh(TORUS);

  //build();
  smooth();
}

void selectSong() {

  selectInput("Select a file to process:", "fileSelected");
}


void initGUI() {

  gui=new USimpleGUI(this);
  gui.addButton("selectSong").setCaption("selectSong","Select Song");
  gui.addButton("playSong").setCaption("playSong","Play");
  gui.addButton("pauseSong").setCaption("pauseSong","Pause");
  gui.newRow();
  gui.addButton("createCube").setCaption("createCube","Cube");
  gui.addButton("createCylinder").setCaption("createCylinder","Cylinder");
  gui.addButton("createTorus").setCaption("createTorus","Torus");
  gui.addButton("createSphere").setCaption("createSphere","Sphere");
  gui.newRow();
  gui.addButton("modifyInsertRandomHoles").setCaption("modifyInsertRandomHoles","Holes");
  gui.addButton("modifyUsingLattice").setCaption("modifyUsingLattice","Lattice");
  gui.addButton("modifyUsingCatmull").setCaption("modifyUsingCatmull","Smooth");
  gui.newRow();
  gui.addButton("saveSTL").setCaption("saveSTL","Export STL");
  gui.addButton("importSTL").setCaption("importSTL","Import STL");
  gui.newRow();
  gui.addButton("savePDF").setCaption("savePDF","Export PDF");
  gui.addButton("saveDXF").setCaption("saveDXF","Export DXF");
  gui.newRow();
  gui.addButton("reset").setCaption("reset","Reset");
  gui.addToggle("toggleWireframe",false).setCaption("toggleWireframe","Wireframe");
  gui.setLayout(false);

}

void reset() {

  song = null;
  addBaseMesh(TORUS);

}

void toggleWireframe(boolean v) {

  wireframe = v;

}

void importSTL() {

  selectInput("Select an STL file:", "stlSelected");
}

void stlSelected(File selection) {

    createFromSTL(selection.getAbsolutePath());


}

void createFromSTL(String path) {

  HEC_FromBinarySTLFile creator  = new HEC_FromBinarySTLFile(path);
  mesh = new HE_Mesh(creator);
  HET_Diagnosis.validate(mesh);

}



void addBaseMesh(int type) {

  if (type==TORUS){
    HEC_Torus creator=new HEC_Torus();
    creator.setRadius(150,200);
    creator.setTubeFacets(8);
    creator.setTorusFacets(64);
    creator.setTwist(3);
    mesh=new HE_Mesh(creator);
  }

  if (type==CYLINDER){
    HEC_Cylinder creator=new HEC_Cylinder();
    creator.setRadius(75,75); 
    creator.setHeight(400);
    creator.setFacets(50).setSteps(12);
    creator.setCap(true,true);// cap top, cap bottom?
  //Default axis of the cylinder is (0,1,0). To change this use the HEC_Creator method setZAxis(..).
    creator.setZAxis(0,1,1);
    mesh=new HE_Mesh(creator); 
  }

  if (type==CUBE){
    HEC_Box creator=new HEC_Box();
    creator.setWidth(200).setHeight(200).setDepth(200); 
    creator.setWidthSegments(8).setHeightSegments(8).setDepthSegments(8);
    mesh=new HE_Mesh(creator);
  }

  if (type==SPHERE){

    HEC_Sphere creator=new HEC_Sphere();
    creator.setRadius(200);
    creator.setUFacets(16);
    creator.setVFacets(16);
    mesh=new HE_Mesh(creator);

  }
 
  HET_Diagnosis.validate(mesh);
  render=new WB_Render(this);

}

void createCube(){

  addBaseMesh(CUBE);

}

void createCylinder(){

  addBaseMesh(CYLINDER);

}

void createTorus(){

  addBaseMesh(TORUS);

}

void createSphere() {

  addBaseMesh(SPHERE);

}

void fileSelected(File selection) {
  if (selection == null) {
    //exit();
  } else {
    initSongWithFile(selection.getAbsolutePath());
  }
}

void initSongWithFile(String file){
  loadFile(file);
  meta = song.getMetaData();
  bufferSize = song.bufferSize();
  //addBaseMesh();
  initFFT();
}

void initFFT() {

  fft = new FFT(song.bufferSize(), song.sampleRate());
}

int songLength() {

  return song.length();

}

int songPosition() {

  return song.position();
}

void loadFile(String file){

  song = minim.loadFile(file, 512);
}

void playSong() {
  if (song != null) {
    song.play();
    isPaused = false;
  }

}

void pauseSong() {
  if (song != null) {
    song.pause();
    isPaused = true;
  }
}

PVector getCartesian(float x, float y, float z) {

   float a = (float) (x * cos(z));
   float xx = (float) (a * cos(y));
   float yy = (float) (x * sin(z));
   float zz = (float) (a * sin(y));
   x = xx;
   y = yy;
   z = zz;
   PVector p = new PVector(x,y,z);
   return p;


}

void processSong() {

  WB_Point3d[] points=mesh.getVerticesAsPoint();
  int limit = (int)map(songPosition(),0,songLength(),0,points.length);
  int start = max(0,limit-3);
  for (int i=start; i < limit; i++) {
    int ampIndex = (int)map(i,start,limit,0,bufferSize);
    float amp = map(song.mix.get(ampIndex),0,1.0,1,1.10);
    points[i] = points[i].scale(amp);
  }


}

int getPercentageProcessed() {
  float p = map(songPosition(),0,songLength()-1,1,100);
  return ceil(p);

}

void smoothMesh() {

  mesh = convertMeshToCatmullClark(mesh);

}




HE_Mesh convertMeshToConvexHull(HE_Mesh m) {

  HEC_ConvexHull creator=new HEC_ConvexHull();
  WB_Point3d[] p = m.getVerticesAsPoint();
  creator.setPoints(p);
  //creator.setUseQuickHull(true);
  HE_Mesh convexHullMesh =new HE_Mesh(creator);
  return convexHullMesh;


}

HE_Mesh convertMeshToCatmullClark(HE_Mesh m) {

  HES_CatmullClark subdividor=new HES_CatmullClark();
  subdividor.setKeepBoundary(true);// preserve position of vertices on a surface boundary
  subdividor.setKeepEdges(true);// preserve position of vertices on edge of selection (only useful if using subdivideSelected)
  HE_Mesh cat = new HE_Mesh();
  cat = m;
  return cat.subdivide(subdividor,1);

}

void extrudeMesh(HE_Mesh m) {

  HEM_Extrude modifier=new HEM_Extrude();
  modifier.setDistance(10);// extrusion distance, set to 0 for inset faces
  modifier.setRelative(false);// treat chamfer as relative to face size or as absolute value
  modifier.setChamfer(2);// chamfer for non-hard edges
  modifier.setHardEdgeChamfer(2);// chamfer for hard edges
  modifier.setThresholdAngle(1.5*HALF_PI);// treat edges sharper than this angle as hard edges
  modifier.setFuse(true);// try to fuse planar adjacent planar faces created by the extrude
  modifier.setFuseAngle(0.05*HALF_PI);// threshold angle to be considered coplanar
  modifier.setPeak(true);//if absolute chamfer is too large for face, create a peak on the face
  m.modify(modifier);

}

void twistMesh(HE_Mesh m) {

  HE_Selection selection=new HE_Selection(mesh);

  Iterator<HE_Face> faceItr=mesh.fItr();

  HE_Face face;
    while(faceItr.hasNext()){
      face=faceItr.next();
      if(random(100) < 50){
        selection.add(face);
      }
    }

   HEM_Twist modifier=new HEM_Twist();


 WB_Line L=new WB_Line(100,0,0,100,0,1);
  modifier.setTwistAxis(L);// Twist axis
  //you can also pass the line as two points:
  //modifier.setBendAxis(0,0,-200,1,0,-200);

  modifier.setAngleFactor(.1);// Angle per unit distance (in degrees) to the twist axis
  // points which are a distance d from the axis are rotated around it by an angle d*angleFactor;

  m.modifySelected(modifier,selection);
}


HE_Mesh convertMeshToLattice(HE_Mesh m) {

  HE_Selection selection=new HE_Selection(m);

  Iterator<HE_Face> faceItr=m.fItr();
  HE_Face face;
  while(faceItr.hasNext()){
   face=faceItr.next();
   if(random(100) < 55){
     selection.add(face);
   }
  }
  HEM_Lattice lattice = new HEM_Lattice().setDepth(50.0).setWidth(50.0);
  m.modifySelected(lattice, selection);

m.cleanUnusedElementsByFace(); //keep only mesh elements belonging to faces
  m.capHalfedges();


  return m;

}

void modifyInsertRandomHoles() {

  HE_Selection selection=new HE_Selection(mesh);

  Iterator<HE_Face> faceItr=mesh.fItr();

  HE_Face face;
    while(faceItr.hasNext()){
      face=faceItr.next();
      if(random(100) < 50){
        mesh.deleteFace(face);
      }
    }
 mesh.cleanUnusedElementsByFace(); //keep only front elements belonging to faces
 mesh.capHalfedges();
   // mesh.resolvePinchPoints();
  mesh.pairHalfedges();

}

HE_Mesh convertMeshToSweepTube(HE_Mesh m) {

WB_Point3d[] p = m.getVerticesAsPoint();

WB_BSpline C=new WB_BSpline(p, 4);

  HEC_SweepTube creator=new HEC_SweepTube();
  creator.setCurve(C);//curve should be a WB_BSpline
  creator.setRadius(20);
  creator.setSteps(40);
  creator.setFacets(8);
  creator.setCap(true, true); // Cap start, cap end?

  HE_Mesh tube=new HE_Mesh(creator);
  return tube;
}

HE_Mesh convertMeshToWireframe(HE_Mesh m) {

  HEM_Wireframe modifier=new HEM_Wireframe();
  modifier.setStrutRadius(6);// strut radius
  modifier.setStrutFacets(6);// number of faces in the struts, min 3, max whatever blows up the CPU
 // modifier.setMaximumStrutOffset(20);// limit the joint radius by decreasing the strut radius where necessary. Joint offset is added after this limitation.
  modifier.setAngleOffset(0.5);// rotate the struts by a fraction of a facet. 0 is no rotation, 1 is a rotation over a full facet. More noticeable for low number of facets.
  modifier.setTaper(false);// allow struts to have different radii at each end?
  m.modify(modifier);
  return m;
}

void modifyUsingLattice() {

  HE_Selection selection=new HE_Selection(mesh);

  Iterator<HE_Face> faceItr=mesh.fItr();

  HE_Face face;
    while(faceItr.hasNext()){
      face=faceItr.next();
      if(random(100) < 50){
        selection.add(face);
      }
    }
    HEM_Lattice lattice = new HEM_Lattice().setDepth(20.0).setWidth(5.0);
    try {
      mesh.modifySelected(lattice, selection);
      mesh.cleanUnusedElementsByFace();
      mesh.capHalfedges();
      originalMesh = mesh.get();
    } catch (Exception e) {

    }




}

void modifyUsingCatmull() {



  HES_CatmullClark subdividor=new HES_CatmullClark();
  subdividor.setKeepBoundary(true);// preserve position of vertices on a surface boundary
  subdividor.setKeepEdges(true);// preserve position of vertices on edge of selection (only useful if using subdivideSelected)
  HE_Mesh cat = new HE_Mesh();
  cat = mesh.get();
  try {
    cat.subdivide(subdividor,1);
    mesh = cat.get();

  } catch (Exception e) {

  }

}




void draw() {


 if (song != null) {
   if (song.isPlaying()) {
     int percentageDone = getPercentageProcessed();
     if (percentageDone >= 99) {
        song.pause();
        isPaused = true;
        println("Done");
        originalMesh = mesh.get();
        //saveSTL();
        //exit();
     } else  {

    processSong();


        }

    }
}


  background(200);
  gui.draw();
  if (record) {
    String fileName = getFilename()+".pdf";
    beginRaw(PDF, fileName);
  }
  if (recordDXF) {
    String fileName = getFilename()+".dxf";
    beginRaw(DXF, fileName);
  }
  lights();
  nav.doTransforms();

  noStroke();
  if (!wireframe) {
    fill(150,150,0);
    render.drawFaces(mesh);
  }
  stroke(0);
  strokeWeight(1);
  render.drawEdges(mesh);
  if (record) {
    endRaw();
    record = false;
    recordDXF = false;
  }

}

void savePDF() {

  record = true;
}

void saveDXF() {

  recordDXF = true;
}




void saveSTL() {

  String fileName = getFilename();
  saveFrame(fileName+".png");
  HET_Export.saveToSTL(mesh,sketchPath(fileName+".stl"),1.0);

}

String getFilename() {
  String fileName;
if (song != null) {
  fileName = meta.author()+"_"+meta.title();
} else {

  fileName = "output";
}
  return fileName;
}

void keyPressed() {
  if(key=='p') {
    println("p");
    if (isPaused) {
      println("play song");
      //song.rewind();
      playSong();
      isPaused = false;
    } else {

      song.pause();
      isPaused = true;
    }
  }
  if (key=='2') {

  mesh = convertMeshToCatmullClark(mesh);
  }

  if (key=='1') {

    mesh = convertMeshToLattice(mesh);
  }

  if (key=='3') {

    mesh = convertMeshToWireframe(mesh);
  }

  if (key=='e') {

    extrudeMesh(mesh);
  }

  if (key=='t') {

    twistMesh(mesh);
  }
  if(key=='o') {
    mesh = originalMesh.get();

  }
  if(key=='s') {
   saveSTL();
  }
}
